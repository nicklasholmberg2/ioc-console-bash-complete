# ioc-console-bash-complete
Based on input from 
https://iridakos.com/tutorials/2018/03/01/bash-programmable-completion-tutorial.html

**How to install**
Source the script to make it work, e.g. source console-autocomp.bash

If for the current user, add "source console-autocomp.bash" to ~/.bashrc

If for all users, add "source console-autocomp.bash" to /etc/profile

**How to use**
$ console <tab> <tab> # starts the autocomplete, continue to type and double tab to complete the IOC name