#/usr/bin/env bash
_console_ioc_completions()
{
	COMPREPLY=($(compgen -W "$(console -u | awk '{print $1;}')" -- "${COMP_WORDS[1]}"))
}
complete -F _console_ioc_completions console

